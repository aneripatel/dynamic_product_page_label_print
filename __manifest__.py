{
    'name': "Dynamic Product Page Label Print",
    'version': '1.1',
    'category': 'Product',
    'description': """Dynamic Product Page Label Print.""",
    'summary': 'This module is used to Create custom page label template from frontend and print dynamic product page label report.',
    'author': '@nny',
    'website': '',
    "depends": ['sale', 'base', 'purchase', 'stock', 'account', 'mrp'],
    'price': 130, 
    'currency': 'EUR',
    "data": [
        'views/wizard_product_page_report_view.xml',
        'page_reports.xml',
        'security/ir.model.access.csv',
        'views/dynamic_prod_page_rpt.xml',
        'data/design_data.xml'
    ],
    'images': ['static/description/main_screenshot.png'],
    "installable": True,
    'auto_install': False,
}
